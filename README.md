A simple News/RSS Feed reader Android application.

Includes:

* Lazy loading images
* Offline content
* Memory and file based image caching
* Loading the detailed story in WebView
* Multiple news sources

Published to Playstore: https://play.google.com/store/apps/details?id=bulemonkey.newsreader