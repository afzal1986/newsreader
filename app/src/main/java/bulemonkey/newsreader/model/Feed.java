package bulemonkey.newsreader.model;

import java.io.Serializable;
import java.util.Date;

import bulemonkey.newsreader.Constants;

/**
 * Created by afzal on 2/4/17.
 */

public class Feed implements Serializable {

    private String title;
    private String description;
    private String author;
    private Date publishedAt;
    private String detailUrl;
    private String bigImageUrl;
    private String thumbnailImageUrl;

    private Constants.Source source;


    public Feed() {
    }

    public Constants.Source getSource() {
        return source;
    }

    public void setSource(Constants.Source source) {
        this.source = source;
    }

    public String getAuthor() {
        return author;
    }


    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(Date publishedAt) {
        this.publishedAt = publishedAt;
    }

    public String getDetailUrl() {
        return detailUrl;
    }

    public void setDetailUrl(String detailUrl) {
        this.detailUrl = detailUrl;
    }

    public String getBigImageUrl() {
        return bigImageUrl;
    }

    public void setBigImageUrl(String bigImageUrl) {
        this.bigImageUrl = bigImageUrl;
    }

    public String getThumbnailImageUrl() {
        return thumbnailImageUrl;
    }

    public void setThumbnailImageUrl(String thumbnailImageUrl) {
        this.thumbnailImageUrl = thumbnailImageUrl;
    }
}
