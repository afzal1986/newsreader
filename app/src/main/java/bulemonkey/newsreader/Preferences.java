package bulemonkey.newsreader;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Convenience class to mange shared preferences.
 * <p>
 * This is used for keep the offline content but can also be used to save user's preferences or settings.
 */

public class Preferences {

    private static SharedPreferences mSharedPref;
    private final String FILE_NAME = "settings";

    private final String KEY_CACHED_CONTENT = "cached.content";
    private final String KEY_SELECTED_SOURCE = "source";
    private final String KEY_FIRST_USE = "first.use";


    public Preferences(Context context) {
        mSharedPref = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
    }


    public String getCachedContent() {
        String defaultValue = null;
        return mSharedPref.getString(KEY_CACHED_CONTENT, defaultValue);
    }

    public void setCachedContent(String content) {
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putString(KEY_CACHED_CONTENT, content);
        editor.commit();
    }

    public void setSource(Constants.Source source) {
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putString(KEY_SELECTED_SOURCE, source.name());
        editor.commit();
    }

    public Constants.Source getSource() {
        return Constants.Source.valueOf(mSharedPref.getString(KEY_SELECTED_SOURCE, Constants.DEFAULT_SOURCE.name()));
    }

    public boolean getFirstUse() {
        boolean defaultValue = true;
        return mSharedPref.getBoolean(KEY_FIRST_USE, defaultValue);
    }

    public void setFirstUse(boolean firstUse) {
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putBoolean(KEY_FIRST_USE, firstUse);
        editor.commit();
    }
}
