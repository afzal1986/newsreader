package bulemonkey.newsreader.view;

import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

/**
 * This is the base activity that should contain common methods that are to be used
 * throughout the application's activities.
 * <p>
 * This avoids the boiler plate code to be pastes in every Activity class, which also makes it
 * easy to modify.
 */

public class BaseActivity extends AppCompatActivity {

    /**
     * Show a message to the user using toast.
     *
     * @param resId
     */
    public void showMsg(int resId) {
        Toast.makeText(getApplicationContext(), resId, Toast.LENGTH_SHORT).show();
    }
}
