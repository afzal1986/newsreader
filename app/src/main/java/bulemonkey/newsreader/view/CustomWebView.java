package bulemonkey.newsreader.view;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;

/**
 * Custom webview to invoke callbacks on scroll change event
 */

public class CustomWebView extends WebView {

    ScrollCallback mOnScrollChangedCallback;


    public CustomWebView(Context context) {
        super(context);
    }

    public CustomWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        mOnScrollChangedCallback.onScroll(l, t);
    }

    public void setScrollCallback(final ScrollCallback scrollChangedCallback) {
        mOnScrollChangedCallback = scrollChangedCallback;
    }

    /**
     * Callback that the activity can implement if it wants to handle the scroll event
     */
    public static interface ScrollCallback {
        public void onScroll(int l, int t);
    }
}
