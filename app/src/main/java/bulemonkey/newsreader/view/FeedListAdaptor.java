package bulemonkey.newsreader.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import bulemonkey.newsreader.Constants;
import bulemonkey.newsreader.R;
import bulemonkey.newsreader.Util;
import bulemonkey.newsreader.model.Feed;

/**
 * Custom adaptor to display feeds in a list view
 */

public class FeedListAdaptor extends ArrayAdapter<Feed> {

    ImageDownloadManager imageDownloadManager;

    public FeedListAdaptor(Context context, int resource) {
        super(context, resource);
    }

    public FeedListAdaptor(Context context, int resource, List<Feed> objects) {
        super(context, resource, objects);
        imageDownloadManager = new ImageDownloadManager(context);
    }

    /**
     * Update feeds data and UI. This method clears all previous objects held by the adaptor.
     *
     * @param newObjects
     */
    public void updateObjectsIfChanged(List<Feed> newObjects) {

        boolean itemsChanged = true;

        // Compare first object to detect if the feeds have been changed
        if (newObjects != null && newObjects.size() > 0 && newObjects.get(0) != null &&
                getCount() > 0 && getItem(0) != null) {
            if (newObjects.get(0).getTitle().equals(getItem(0).getTitle())) {
                itemsChanged = false;
            }
        }

        if (itemsChanged) {
            clear();
            addAll(newObjects);
            imageDownloadManager.clearCache();
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater layoutInflater;
        layoutInflater = LayoutInflater.from(getContext());

        boolean isTop = false;

        // We pick respective layout based on the position of the item,
        // top layout is for the relatively big image and vertical views.
        if (position == 0) {
            isTop = true;
            convertView = layoutInflater.inflate(R.layout.list_item_top, null);
        } else {
            convertView = layoutInflater.inflate(R.layout.list_item, null);
        }

        Feed feed = getItem(position);

        if (feed != null) {
            TextView titleTxt = (TextView) convertView.findViewById(R.id.title_text_view);
            TextView dateTxt = (TextView) convertView.findViewById(R.id.date_text_view);
            TextView authorTxt = (TextView) convertView.findViewById(R.id.author_text_view);
            ImageView thumbnailImg = (ImageView) convertView.findViewById(R.id.thumbnail_image_view);

            if (titleTxt != null) {
                titleTxt.setText(feed.getTitle());
            }

            if (authorTxt != null) {
                authorTxt.setText(Util.getDisplayAuthor(feed));
            }

            if (dateTxt != null) {
                if (feed.getPublishedAt() != null) {
                    dateTxt.setText(Constants.DISPLAY_DATE_FORMAT.format(feed.getPublishedAt()));
                }
            }


            // Hide the image view if no thumbnail URL
            if (thumbnailImg != null) {

                // If its the top element, we check the big image url, else we check the thumbnail
                // to determine if we need to hide the placeholder image
                if ((!isTop && Util.isEmptyString(feed.getThumbnailImageUrl())) ||
                        (isTop && Util.isEmptyString(feed.getBigImageUrl()))) {
                    thumbnailImg.setVisibility(View.GONE);
                } else {
                    // Display image from the url
                    if (isTop) {
                        imageDownloadManager.showImage(feed.getBigImageUrl(), thumbnailImg);
                    } else {
                        imageDownloadManager.showImage(feed.getThumbnailImageUrl(), thumbnailImg);
                    }
                }
            }

        }

        return convertView;
    }
}
