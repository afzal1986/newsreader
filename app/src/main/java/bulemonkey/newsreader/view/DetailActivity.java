package bulemonkey.newsreader.view;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import bulemonkey.newsreader.Constants;
import bulemonkey.newsreader.R;
import bulemonkey.newsreader.Util;
import bulemonkey.newsreader.model.Feed;

/**
 * Activity to load a detailed story of a selected feed.
 * <p>
 * The feed object must be passe in the intent or else the activity will display an error msg and close.
 */
public class DetailActivity extends BaseActivity {

    // Hold the progress bar view for displaying while the webview page is loading.
    ProgressBar mProgressBar;

    // Text view to display beam up, also used as a button
    TextView mBeamUpTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Customizing toolbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Defining navigation action
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        // Initiate the webview
        final CustomWebView webView = (CustomWebView) findViewById(R.id.web_view);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
        webView.setScrollCallback(new CustomWebView.ScrollCallback() {
            @Override
            public void onScroll(int l, int t) {

                if (t > Constants.SHOW_BEAM_UP_AFTER_SCROLL_OFFSET) {
                    if (mBeamUpTxt.getVisibility() == View.GONE) {
                        mBeamUpTxt.setVisibility(View.VISIBLE);
                        Animations.slideDown(getApplicationContext(), mBeamUpTxt);
                    }
                } else {
                    if (mBeamUpTxt.getVisibility() == View.VISIBLE) {
                        Animations.slideUp(getApplicationContext(), mBeamUpTxt);
                        mBeamUpTxt.setVisibility(View.GONE);
                    }
                }
            }
        });

        // Progress bar
        mProgressBar = (ProgressBar) findViewById(R.id.loading_bar);

        mBeamUpTxt = (TextView) findViewById(R.id.beam_up_txt);
        mBeamUpTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webView.setScrollY(0);
            }
        });


        webView.setWebViewClient(new DetailWebViewClient());


        // Get feed data from the intent
        Feed feed = (Feed) getIntent().getSerializableExtra(Constants.KEY_EXTRA_FEED);

        if (feed != null && !Util.isEmptyString(feed.getDetailUrl())) {
            // Set the title
            setTitle(feed.getTitle());

            // Load the detail url
            webView.loadUrl(feed.getDetailUrl());
        } else {
            // If we can't find the url to load, show error msg and finish this activity
            showMsg(R.string.feed_details_not_found_msg);
            finish();
        }
    }


    /**
     * WebView client to display progress bar while loading the page and also to stop
     * pages being loaded in external applications.
     */
    private class DetailWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return false;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            mProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            mProgressBar.setVisibility(View.GONE);
        }

    }
}
