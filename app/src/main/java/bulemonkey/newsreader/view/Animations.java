package bulemonkey.newsreader.view;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import bulemonkey.newsreader.R;

/**
 * Created by afzal on 2/5/17.
 */

public class Animations {

    public static void slideDown(Context context, View view) {
        Animation animation =
                AnimationUtils.loadAnimation(context, R.anim.slide_down);
        view.startAnimation(animation);
    }

    public static void slideUp(Context context, View view) {
        Animation animation =
                AnimationUtils.loadAnimation(context, R.anim.slide_up);
        view.startAnimation(animation);
    }

    public static void fadeIn(Context context, View view) {
        Animation animation =
                AnimationUtils.loadAnimation(context, R.anim.fade_in);
        view.startAnimation(animation);
    }
}
