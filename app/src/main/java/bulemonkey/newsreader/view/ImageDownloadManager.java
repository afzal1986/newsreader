package bulemonkey.newsreader.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Class to handle the image download and/or display from cache if previously downloaded.
 * <p>
 * This class (currently) only uses the sd card/internal storage as caching however, this can be extended
 * to use memory cache before hitting the storage as well.
 */

public class ImageDownloadManager {


    private static final String LOG_TAG = "ImageDownloadManager";

    // Maximum number of download threads to run at a time
    private static final int MAX_THREADS = 4;

    // File extension & compression quality to store the cached files
    private static final String CACHE_DIR = ".news";
    private static final String FILE_EXT = ".jpg";
    private static final int COMPRESSION_QUALITY = 60;

    // Memory cache for fast retrieval
    private static final int MAX_MEM_SIZE = 15;
    private Map<String, Bitmap> memCache = Collections.synchronizedMap(new HashMap<String, Bitmap>());

    // To run download tasks
    private ExecutorService mExecutorService;

    //To display images on main/UI thread
    private Handler mHandler = new Handler();

    // Store all the image display requests with their urls
    private Map<String, Boolean> serverRequests = Collections.synchronizedMap(new HashMap<String, Boolean>());

    // File storage directory
    private File cacheDir;

    // Application context
    private Context mContext;

    public ImageDownloadManager(Context context) {

        this.mContext = context;

        // Initiate executor
        mExecutorService = Executors.newFixedThreadPool(MAX_THREADS);

        // Initiate storage
        cacheDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES + "/" + CACHE_DIR + "/");
        cacheDir.mkdir();
    }

    /**
     * Display image from cache if available or else
     * download and then display
     *
     * @param imageUrl
     * @param imageView
     */
    public void showImage(String imageUrl, final ImageView imageView) {

        Bitmap memBitmap = memCache.get(imageUrl);
        if (memBitmap != null) {
            imageView.setImageBitmap(memBitmap);
        } else {
            // Start download thread
            mExecutorService.submit(new ImageDownloader(imageUrl, imageView));
        }
    }

    public void clearCache() {
        // Clear file cache
        String[] images = cacheDir.list();
        for (int i = 0; i < images.length; i++) {
            new File(cacheDir, images[i]).delete();
        }

        // Clear memory cache
        memCache.clear();
    }

    /**
     * Gets the image from storage (based on the url) if available else returns null
     *
     * @param url
     * @return bitmap
     */
    private Bitmap getFileFromStorage(String url) {

        Bitmap localBitmap = null;

        try {

            File cacheFile = new File(cacheDir, url.hashCode() + FILE_EXT);

            if (cacheFile.exists()) {
                FileInputStream fileInputStream = new FileInputStream(cacheFile);
                localBitmap = BitmapFactory.decodeStream(fileInputStream);
            }

        } catch (FileNotFoundException exception) {
            Log.i(LOG_TAG, "Cached bitmap not found!");
        }

        return localBitmap;

    }

    /**
     * Stores the given bitmap into the storage.
     *
     * @param bitmap
     * @param url
     */
    private void saveBitmap(Bitmap bitmap, String url) {
        File cacheFile = new File(cacheDir, url.hashCode() + FILE_EXT);
        try {

            // Create a new bitmap file
            cacheFile.createNewFile();
            FileOutputStream fileOutputStream = new FileOutputStream(cacheFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, COMPRESSION_QUALITY, fileOutputStream);

            // Write and close the stream
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (Exception e) {
            Log.e(LOG_TAG, "Error when saving image to cache. ", e);
        }

    }

    /**
     * Adds a bitmap to memory cache. This method also checks for mem cache element size and removes a
     * random element if it has reached the limit.
     * <p>
     * This method can be improved a lot by taking in account the actual size of all the images it holds and least accessed image
     * before choosing a candidate for removal.
     *
     * @param url
     * @param bitmap
     */
    private void addBitmapToMemCache(String url, Bitmap bitmap) {

        // Check if we have reached the limit of our mem cache, if so, remove an item
        if (memCache.size() > MAX_MEM_SIZE) {
            memCache.remove(memCache.keySet().toArray()[0]);
        }

        // Add the new bitmap to mem cache.
        memCache.put(url, bitmap);
    }

    /**
     * This class is used to set the image of an image view with the provided bitmap.
     * This is done here so this class can be used for downloaded bitmaps as well as from cached files.
     */
    class ImagePlacer implements Runnable {
        Bitmap bitmap = null;
        ImageView imageView;

        public ImagePlacer(Bitmap bitmap, ImageView imageView) {
            this.bitmap = bitmap;
            this.imageView = imageView;
        }

        @Override
        public void run() {
            imageView.setImageBitmap(bitmap);
            Animations.fadeIn(mContext, imageView);
        }
    }

    /**
     * The actual downloader task that runs in the executor.
     * If the required image is found cached in the storage, the downloading will be skipped
     * and the cached file will be set to the image view.
     */
    class ImageDownloader implements Runnable {

        String imageUrl = null;
        ImageView imageView;

        public ImageDownloader(String url, ImageView imageView) {
            this.imageUrl = url;
            this.imageView = imageView;
        }

        @Override
        public void run() {

            HttpURLConnection connection = null;


            // Check if we have cached bitmap
            final Bitmap fromStorage = getFileFromStorage(imageUrl);

            // Set the cached bitmap to the image view on main thread
            if (fromStorage != null) {
                mHandler.post(new ImagePlacer(fromStorage, imageView));
                addBitmapToMemCache(imageUrl, fromStorage);
            } else {

                // We need to download the image as it was not found on the storage
                try {
                    if (serverRequests.get(imageUrl) == null) {

                        serverRequests.put(imageUrl, true);

                        URL url = new URL(imageUrl);
                        connection = (HttpURLConnection) url.openConnection();
                        connection.setDoInput(true);
                        connection.connect();
                        InputStream input = connection.getInputStream();
                        final Bitmap downloadedBitmap = BitmapFactory.decodeStream(input);

                        // Keep the file in storage
                        saveBitmap(downloadedBitmap, imageUrl);

                        // Display downloaded image in the image view (on the UI thread)
                        if (downloadedBitmap != null) {
                            mHandler.post(new ImagePlacer(downloadedBitmap, imageView));
                        }

                        addBitmapToMemCache(imageUrl, downloadedBitmap);
                        serverRequests.remove(imageUrl);
                    }

                } catch (Exception exception) {
                    exception.printStackTrace();
                } finally {
                    connection.disconnect();
                }
            }
        }
    }

}
