package bulemonkey.newsreader.view;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;

import org.json.JSONException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import bulemonkey.newsreader.Constants;
import bulemonkey.newsreader.FeedParser;
import bulemonkey.newsreader.Preferences;
import bulemonkey.newsreader.R;
import bulemonkey.newsreader.comms.NetworkCallback;
import bulemonkey.newsreader.comms.NewsDownloaderTask;
import bulemonkey.newsreader.model.Feed;

/**
 * Activity to display news feeds in a list view
 */
public class MainActivity extends BaseActivity implements NetworkCallback {

    private final String LOG_TAG = "MainActivity";

    // Adaptor to hold downloaded/cached news feeds and construct
    // list view
    FeedListAdaptor mFeedListAdaptor;

    // Async task to download and parse feeds
    NewsDownloaderTask downloaderTask;

    // Holds progress bar view
    ProgressBar mProgressBar;

    // Holds error image view that should be displayed if
    // there are no cached feeds and we encounter a network error.
    ImageView mErrorImage;

    // Preferences for offline content and/or user settings
    Preferences preferences;

    // To avoid multiple update requests
    boolean isUpdating = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        preferences = new Preferences(getApplicationContext());

        final ListView feedsListView = (ListView) findViewById(R.id.feeds_list_view);
        mProgressBar = (ProgressBar) findViewById(R.id.loading_bar);
        mErrorImage = (ImageView) findViewById(R.id.error_img);

        // Getting offline content if available
        String offlineContent = preferences.getCachedContent();

        List<Feed> localFeeds = new ArrayList<Feed>();
        if (offlineContent != null) {
            try {
                localFeeds = FeedParser.parseJSONResponse(offlineContent, preferences.getSource());
                mProgressBar.setVisibility(View.GONE);
            } catch (JSONException e) {
                Log.e(LOG_TAG, "JSON Error parsing offline content! " + e.getMessage());
                e.printStackTrace();
            } catch (ParseException e) {
                Log.e(LOG_TAG, "Error parsing offline content! " + e.getMessage());
                e.printStackTrace();
            }
        }

        mFeedListAdaptor = new FeedListAdaptor(getApplicationContext(), R.layout.list_item, localFeeds);
        feedsListView.setAdapter(mFeedListAdaptor);

        feedsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startDetailActivity(mFeedListAdaptor.getItem(position));
            }
        });

        updateTitle();

        if (preferences.getFirstUse()) {
            showWelcomeDialog();
            preferences.setFirstUse(false);
        }

        fetchNews();
    }

    private void updateTitle() {
        setTitle(getString(R.string.main_title) + " - " + preferences.getSource().sourceName);
    }

    private void startDetailActivity(Feed feed) {
        Intent detailActivityIntent = new Intent(this, DetailActivity.class);
        detailActivityIntent.putExtra(Constants.KEY_EXTRA_FEED, feed);
        startActivity(detailActivityIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_select_source) {
            showNewsSourceSelector();
            return true;
        }
        else if (id == R.id.action_refresh) {
            fetchNews();
            return true;
        }
        else if (id == R.id.action_about) {
            showAboutDialog();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showNewsSourceSelector() {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        CharSequence items[] = Constants.Source.sourceNames();
        adb.setSingleChoiceItems(items, preferences.getSource().ordinal(), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface d, int n) {
                preferences.setSource(Constants.Source.values()[n]);
                fetchNews();
                updateTitle();
                d.dismiss();
            }

        });
        adb.setNegativeButton(R.string.cancel, null);
        adb.setTitle(R.string.select_source);
        adb.show();
    }

    private void showAboutDialog() {
        String versionName = "";

        try {
            versionName = getPackageManager()
                    .getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setPositiveButton(R.string.ok, null);
        adb.setTitle(getString(R.string.version) + " " + versionName);
        adb.setMessage(R.string.about_msg);
        adb.show();
    }

    private void showWelcomeDialog() {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setPositiveButton(R.string.ok, null);
        adb.setTitle(getString(R.string.welcome));
        adb.setMessage(R.string.welcome_msg);
        adb.show();
    }

    private void showNetworkError() {
        Snackbar.make(findViewById(android.R.id.content), R.string.network_error_msg, Snackbar.LENGTH_LONG)
                .setAction(R.string.retry, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        fetchNews();
                    }
                })
                .show();
    }

    private void fetchNews() {
        if (!isUpdating) {
            isUpdating = true;
            downloaderTask = new NewsDownloaderTask(getApplicationContext(), this);
            downloaderTask.execute();
        }
    }


    @Override
    public void startingDownload() {
        // We don't have any data yet, safe to display progress bar
        if (mFeedListAdaptor.getCount() <= 0) {
            mProgressBar.setVisibility(View.VISIBLE);
        }
        showMsg(R.string.updating_msg);
    }

    @Override
    public void onDownloadSuccess(List<Feed> fetchedFeeds) {
        isUpdating = false;
        mProgressBar.setVisibility(View.GONE);
        mErrorImage.setVisibility(View.GONE);
        mFeedListAdaptor.updateObjectsIfChanged(fetchedFeeds);
    }

    @Override
    public void onDownloadError(Exception e) {
        isUpdating = false;
        if (mFeedListAdaptor.getCount() <= 0) {
            mErrorImage.setVisibility(View.VISIBLE);
        }
        mProgressBar.setVisibility(View.GONE);
        showNetworkError();
    }
}
