package bulemonkey.newsreader;

import bulemonkey.newsreader.model.Feed;

/**
 * Common util methods
 */

public class Util {

    /**
     * Method to check if a string is null or empty
     *
     * @param string
     * @return boolean
     */
    public static boolean isEmptyString(String string) {
        if (string == null || string.trim().equals("") || string.equals("null")) {
            return true;
        }

        return false;
    }

    /**
     * Gets the author content if it doesn't contain a link or is not null,
     * else returns the feed's source name for display.
     *
     * @param feed
     * @return String
     */
    public static String getDisplayAuthor(Feed feed) {

        String author = feed.getAuthor();

        if (author == null || author.trim().length() <= 0 || author.length() > 20 || author.contains("http") || author.equals("null")) {
            author = feed.getSource().sourceName;
        }

        return author;
    }

}
