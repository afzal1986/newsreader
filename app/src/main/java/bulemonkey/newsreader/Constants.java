package bulemonkey.newsreader;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

/**
 * Contains common constants of the application.
 *
 * It is recommended to keep all members static.
 */

public class Constants {

    public static final String FEED_URL = "https://newsapi.org/v1/articles?sortBy=top&apiKey=3651a5ddc1c1405ebaf8c8845097428e&source=";

    public static final Source DEFAULT_SOURCE = Source.CNN;

    public static final SimpleDateFormat DISPLAY_DATE_FORMAT = new SimpleDateFormat("MMM dd, yyyy hh:mm aaa");

    public static final String KEY_EXTRA_FEED = "com.bluemonkey.feed";

    public static final int SHOW_BEAM_UP_AFTER_SCROLL_OFFSET = 250;

    // JSON key to parse inbound payload
    public static final String JSON_KEY_ITEMS               = "articles";
    public static final String JSON_KEY_TITLE               = "title";
    public static final String JSON_KEY_PUB_DATE            = "publishedAt";
    public static final String JSON_KEY_LINK                = "url";
    public static final String JSON_KEY_THUMBNAIL           = "urlToImage";
    public static final String JSON_KEY_DESC                = "description";
    public static final String JSON_KEY_AUTHOR              = "author";
    public static final SimpleDateFormat JSON_DATE_FORMAT   = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

    static {
        JSON_DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("GMT"));
    }

    public static enum Source {

        ABC("ABC News", "abc-news-au"),
        BBC ("BBC News", "bbc-news"),
        CNN("CNN", "cnn"),
        NYT("The New York Times", "the-new-york-times"),
        DAILY_MAIL("Daily Mail", "daily-mail"),
        MASHABLE("Mashable", "mashable");

        public String sourceName;
        public String sourceCode;

        Source(String name, String code) {
            this.sourceCode = code;
            this.sourceName = name;
        }

        public static String[] sourceNames() {
            Source[] sources = values();
            String[] names = new String[sources.length];

            for (int i = 0; i < sources.length; i++) {
                names[i] = sources[i].sourceName;
            }

            return names;
        }
    }



}
