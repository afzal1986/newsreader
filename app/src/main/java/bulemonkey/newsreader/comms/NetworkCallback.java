package bulemonkey.newsreader.comms;

import java.util.List;

import bulemonkey.newsreader.model.Feed;

/**
 * Call back interface to wrap around Async task which makes the network connection to download
 * news feeds. This makes the response handling easier for the activity which can focus on UI.
 */

public interface NetworkCallback {

    /**
     * Gets called when the download task is about to start.
     * This is called on main thread, so its safe to do UI operations.
     */
    void startingDownload();

    /**
     * Gets called when the download of feeds is successfully completed.
     *
     * @param feeds
     */
    void onDownloadSuccess(List<Feed> feeds);

    /**
     * Gets called if there is an error while connecting, downloading or parsing feeds.
     *
     * @param exception
     */
    void onDownloadError(Exception exception);
}
