package bulemonkey.newsreader.comms;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import bulemonkey.newsreader.Constants;
import bulemonkey.newsreader.FeedParser;
import bulemonkey.newsreader.Preferences;
import bulemonkey.newsreader.model.Feed;

/**
 * Network task to connect, download and parse feeds.
 * <p>
 * The callback methods are called on the main thread so its always safe to do UI operations on them.
 */

public class NewsDownloaderTask extends AsyncTask<Void, Void, List<Feed>> {

    enum ResultCode {
        SUCCESS,
        FAIL
    }

    private static final String LOG_TAG = "NewsDownloaderTask";
    private NetworkCallback mNetworkCallback;

    Preferences preferences;

    /**
     * Recording any exception that occurred while working in background
     * which needs to be passed to the callback.
     */
    private Exception mException;

    /**
     * Custom result code to record the success or failure of the network operation. This is checked
     * to determine which callback method gets called on the main thread after background task is finished.
     */
    private ResultCode mResultCode;

    public NewsDownloaderTask(Context context, NetworkCallback mNetworkCallback) {
        this.mNetworkCallback = mNetworkCallback;
        preferences = new Preferences(context);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mNetworkCallback.startingDownload();
    }

    @Override
    protected List<Feed> doInBackground(Void... params) {
        HttpURLConnection httpUrlConnection = null;
        BufferedReader bufferedReader = null;
        List<Feed> feeds = null;

        try {

            // Generate URL from selected news source
            Constants.Source source = preferences.getSource();
            String urlString = Constants.FEED_URL + source.sourceCode;

            // Get the URL from the parameter
            URL url = new URL(urlString);

            Log.i(LOG_TAG, "Performing HTTP get: " + url);

            // Perform http request
            httpUrlConnection = (HttpURLConnection) url.openConnection();
            httpUrlConnection.setRequestMethod("GET");
            httpUrlConnection.connect();

            // Read the input stream into a String
            int responseCode = httpUrlConnection.getResponseCode();

            Log.i(LOG_TAG, "HTTP response: " + responseCode);

            if (responseCode == HttpURLConnection.HTTP_OK) {

                // Response code is 200 (OK), good to continue reading
                InputStream inputStream = httpUrlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();

                if (inputStream != null) {

                    bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                    // Read and construct response payload line by line
                    String responseLine;
                    while ((responseLine = bufferedReader.readLine()) != null) {
                        buffer.append(responseLine);
                    }

                    if (buffer.length() > 0) {
                        String responsePayload = buffer.toString();
                        feeds = FeedParser.parseJSONResponse(responsePayload, source);
                        mResultCode = ResultCode.SUCCESS;

                        // Save the downloaded content into shared preference for offline access
                        preferences.setCachedContent(responsePayload);
                    } else {
                        throw new Exception("Buffer reader length 0!");
                    }
                } else {
                    throw new Exception("Input stream null!");
                }
            } else {
                throw new Exception("Response code not 200: " + responseCode);
            }

            // An error occurred while connecting
        } catch (IOException e) {

            Log.e(LOG_TAG, "Network IO Error: " + e.getMessage());
            onError(e);
            e.printStackTrace();

            // An error occurred while parsing JSON
        } catch (JSONException e) {

            Log.e(LOG_TAG, "JSON Parsing Error: " + e.getMessage());
            onError(e);
            e.printStackTrace();

            // Generic error
        } catch (Exception e) {

            Log.e(LOG_TAG, "Network Error: " + e.getMessage());
            onError(e);
            e.printStackTrace();

        } finally {

            // Close the connection and reader
            if (httpUrlConnection != null) {
                httpUrlConnection.disconnect();
            }

            if (bufferedReader != null) {

                try {

                    bufferedReader.close();

                    // An error occurred while closing the reader
                } catch (final IOException e) {
                    Log.e(LOG_TAG, "Network error while closing reader: " + e.getMessage());
                    onError(e);
                }
            }
        }

        return feeds;
    }


    /**
     * Mark the network operation as a failure with an exception
     *
     * @param exception
     */
    private void onError(Exception exception) {
        mException = exception;
        mResultCode = ResultCode.FAIL;
    }

    @Override
    protected void onPostExecute(List<Feed> feeds) {
        super.onPostExecute(feeds);

        switch (mResultCode) {
            case SUCCESS:
                mNetworkCallback.onDownloadSuccess(feeds);
                break;
            case FAIL:
                mNetworkCallback.onDownloadError(mException);
                break;
        }

    }
}
