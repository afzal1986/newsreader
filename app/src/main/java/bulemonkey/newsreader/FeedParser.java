package bulemonkey.newsreader;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import bulemonkey.newsreader.model.Feed;

/**
 * Class to parse JSON string containing news feeds
 */

public class FeedParser {

    private static final String LOG_TAG = "FeedParser";

    public static List<Feed> parseJSONResponse(String jsonString, Constants.Source source) throws JSONException, ParseException {

        List<Feed> feeds = new ArrayList<Feed>();

        if (jsonString != null) {
            JSONObject payloadJSON = new JSONObject(jsonString);

            // Get the feeds array
            if (payloadJSON.has(Constants.JSON_KEY_ITEMS)) {
                JSONArray feedsJSONArray = payloadJSON.getJSONArray(Constants.JSON_KEY_ITEMS);

                // Parse individual feeds
                for (int i = 0; i < feedsJSONArray.length(); i++) {

                    JSONObject feedJSON = feedsJSONArray.getJSONObject(i);

                    Feed feed = new Feed();

                    if (feedJSON.has(Constants.JSON_KEY_TITLE)) {
                        feed.setTitle(feedJSON.getString(Constants.JSON_KEY_TITLE));
                    }

                    if (feedJSON.has(Constants.JSON_KEY_DESC)) {
                        feed.setDescription(feedJSON.getString(Constants.JSON_KEY_DESC));
                    }

                    if (feedJSON.has(Constants.JSON_KEY_AUTHOR) && !feedJSON.get(Constants.JSON_KEY_AUTHOR).equals(JSONObject.NULL)) {
                        String author = feedJSON.getString(Constants.JSON_KEY_AUTHOR);
                        if (!Util.isEmptyString(author)) {
                            feed.setAuthor(author);
                        }
                    }

                    if (feedJSON.has(Constants.JSON_KEY_LINK)) {
                        feed.setDetailUrl(feedJSON.getString(Constants.JSON_KEY_LINK));
                    }

                    if (feedJSON.has(Constants.JSON_KEY_THUMBNAIL)) {
                        feed.setThumbnailImageUrl(feedJSON.getString(Constants.JSON_KEY_THUMBNAIL));
                        feed.setBigImageUrl(feedJSON.getString(Constants.JSON_KEY_THUMBNAIL));
                    }

                    if (feedJSON.has(Constants.JSON_KEY_PUB_DATE)) {
                        // Convert the date from JSON
                        try {
                            String dateString = feedJSON.getString(Constants.JSON_KEY_PUB_DATE);
                            if (!Util.isEmptyString(dateString)) {
                                feed.setPublishedAt(Constants.JSON_DATE_FORMAT.parse(dateString));
                            }
                        } catch (ParseException ex) {
                            Log.e("Error parsing date: ", ex.getMessage());
                            ex.printStackTrace();
                        }
                    }

                    feed.setSource(source);

                    feeds.add(feed);
                }
            }
        }

        return feeds;
    }
}
