package bulemonkey.newsreader;

import org.junit.Test;

import java.util.Calendar;
import java.util.List;

import bulemonkey.newsreader.model.Feed;

import static junit.framework.Assert.assertEquals;

/**
 * Created by afzal on 2/5/17.
 */

public class JSONParserTest {

    String jsonString = "{\"status\":\"ok\",\"source\":\"abc-news-au\",\"sortBy\":\"top\",\"articles\":[{\"author\":null,\"title\":\"'Too late to leave' some areas as bushfire emergency grips NSW\",\"description\":\"Properties are under threat as NSW RFS issues emergency warnings for blazes at Dondingalong, Kains Flat, Leadville near Dunedoo, and Beechwood.\",\"url\":\"http://www.abc.net.au/news/2017-02-12/people-told-to-leave-properties-as-bushfire-emergency-grips-nsw/8263674\",\"urlToImage\":\"http://www.abc.net.au/news/image/8263614-1x1-700x700.jpg\",\"publishedAt\":\"2017-02-12T09:52:33Z\"}]}";
    Constants.Source source = Constants.Source.ABC;

    @Test
    public void jsonValidator_correctFeedsSize() throws Exception {
        List<Feed> feeds = FeedParser.parseJSONResponse(jsonString, source);
        assertEquals(1,feeds.size());
    }

    @Test
    public void jsonValidator_correctFeedData() throws Exception {
        List<Feed> feeds = FeedParser.parseJSONResponse(jsonString, source);
        Feed feed = feeds.get(0);

        // Test title
        assertEquals("'Too late to leave' some areas as bushfire emergency grips NSW",feed.getTitle());

        // Test date parsing should be 2017-02-12T09:52:33Z
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(2017, Calendar.FEBRUARY, 12, 20, 52, 33);
        assertEquals(calendar.getTimeInMillis(),feed.getPublishedAt().getTime());

        // Test thumbnail url
        assertEquals("http://www.abc.net.au/news/image/8263614-1x1-700x700.jpg",feed.getThumbnailImageUrl());

        // Test big thumbnail url
        assertEquals("http://www.abc.net.au/news/image/8263614-1x1-700x700.jpg",feed.getBigImageUrl());

        // Test detail url
        assertEquals("http://www.abc.net.au/news/2017-02-12/people-told-to-leave-properties-as-bushfire-emergency-grips-nsw/8263674",feed.getDetailUrl());

        // Test desc
        assertEquals("Properties are under threat as NSW RFS issues emergency warnings for blazes at Dondingalong, Kains Flat, Leadville near Dunedoo, and Beechwood.",feed.getDescription());
    }

}
