package bulemonkey.newsreader;

import org.junit.Test;

import bulemonkey.newsreader.model.Feed;

import static junit.framework.Assert.assertEquals;
/**
 * Created by afzal on 2/5/17.
 */

public class UtilTest {

    @Test
    public void utilTest_correctEmptyString() throws Exception {
        assertEquals(true, Util.isEmptyString(""));
        assertEquals(true, Util.isEmptyString(null));
        assertEquals(true, Util.isEmptyString(" "));
        assertEquals(false, Util.isEmptyString("test"));
    }

    @Test
    public void utilTest_correctDisplaySource() throws Exception {

        Feed feed = new Feed();
        feed.setSource(Constants.Source.ABC);

        feed.setAuthor(null);
        assertEquals(Constants.Source.ABC.sourceName, Util.getDisplayAuthor(feed));

        feed.setAuthor("http://www.google.com");
        assertEquals(Constants.Source.ABC.sourceName, Util.getDisplayAuthor(feed));

        feed.setAuthor(" ");
        assertEquals(Constants.Source.ABC.sourceName, Util.getDisplayAuthor(feed));

        feed.setAuthor("null");
        assertEquals(Constants.Source.ABC.sourceName, Util.getDisplayAuthor(feed));

        feed.setAuthor("CNN");
        assertEquals("CNN", Util.getDisplayAuthor(feed));
    }

}
